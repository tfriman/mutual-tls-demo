#! /usr/bin/env bash

# Runs all linked docker containers

cd `dirname $0`
cd ..

base_dir=`pwd`

# Run "docker build . -t python-dumper" in ../docker/python-dumper
docker run -p 8000:8000 --name python-dumper -d python-dumper

docker run --name mutual-tls-nginx --link python-dumper:python-dumper -p 9443:443 -v $base_dir/docker/nginx/nginx.conf:/etc/nginx/nginx.conf:ro -v $base_dir/keys/valid:/certs:ro -d nginx:1.13.8
