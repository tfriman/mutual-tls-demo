# Mutual TLS demo project

## Scope and disclaimer

This document describes how to set up demo Mutual TLS (also known as
2-way TLS) using Docker, Nginx and openssl. This is not meant to be
used in production but just to provide a working proof-of-concept. In
addition, the writer of this document is not a security nor server
configuration expert so there may be huge holes in this set up. If you
find one, PR is much appreciated.

Nor does this document describe how the most difficult part of the
MTLS should be handled, namely the certificate governance; issuing
using proper root CA, handling the expiration, handling the
revocation of certificates etc.

One more warning: do not use any of the keys in this project anywhere
else.

### MTLS brief background

Mutual TLS means that not only the caller of the server identifies the
server identity but in addition server verifies the callee. The flow
starts as 1-way TLS but during the handshake server asks for client to
provide certificate. TODO: add link to more thorough description.

## Versions

This was tested with MacOS 10.13, openssl LibreSSL 2.2.7, Docker
version 17.09.0-ce. Where applicable, Docker base image fixed version
is used instead of latest.

## Certificate generation

## PoC setup

This PoC consists of nginx reverse proxy which will do the TLS termination and add two headers to forwarded requests:

-   X-client-cert-fingerprint will contain client certificate's fingerprint

-   X-client-cert-dn will contain the “subject DN” string of the client certificate for an established SSL connection according to RFC 2253

Downstream service to be called is a simple python script which will
check that request has "X-client-cert-fingerprint" header and acts
accordingly.

## Nginx configuration

[nginx.conf](./docker/nginx/nginx.conf)

## python downstream server

[Dockerfile](./docker/python-dumper/Dockerfile)

[server.py](./docker/python-dumper/server.py)

# Key generation

## Pre-generated keys

There are certificates generated using instructions below so if you
wish you can use those. Note: never use these keys anywhere else.
There are expired client and server certificates under invalid
directory for you to see how this behaves with expired keys. valid
directory contains keys which are valid until year 2028.

    openssl genpkey -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -aes-128-cbc -out ca.key

## Commands

    openssl req -new -x509 -days 3650 -key ca.key -out ca.crt

Here are the values used:

    Country Name (2 letter code) []:FI
    State or Province Name (full name) []:
    Locality Name (eg, city) []:Helsinki
    Organization Name (eg, company) []:DemoSoft corp
    Organizational Unit Name (eg, section) []:poc
    Common Name (eg, fully qualified host name) []:demosoft.invalid
    Email Address []:demosoft@invalid

    openssl genrsa -des3 -out client.key 4096

pass phrase: client

    openssl req -new -key client.key -out client.csr

Here are the values used:

    Country Name (2 letter code) []:FI
    State or Province Name (full name) []:
    Locality Name (eg, city) []:Helsinki
    Organization Name (eg, company) []:DemoClient corp
    Organizational Unit Name (eg, section) []:client poc
    Common Name (eg, fully qualified host name) []:democlient.invalid
    Email Address []:democlient@invalid

    Please enter the following 'extra' attributes
    to be sent with your certificate request
    A challenge password []:

And generate client certificate:

    openssl x509 -req -days 3650 -in client.csr -CA ca.crt -CAkey ca.key -set_serial 01 -out client.crt

root ca password is root

    openssl pkcs12 -export -clcerts -in client.crt -inkey client.key -out client.p12

password for client is client, empty export password used

    openssl pkcs12 -in client.p12 -out client.pem -clcerts

empty import password, pem pass phrase: foobar

    openssl pkcs12 -in client.p12 -out client-key.pem -nocerts

empty import password, pem pass phrase: barfoo

    openssl verify -verbose -CAfile ca.crt client.crt

Should report OK

## Generate server cert

    openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout nginx.key -out nginx.crt

Values used:

    Country Name (2 letter code) []:FI
    State or Province Name (full name) []:
    Locality Name (eg, city) []:Helsinki
    Organization Name (eg, company) []:DemoServerOrg
    Organizational Unit Name (eg, section) []:demo server unit
    Common Name (eg, fully qualified host name) []:demoserver.invalid
    Email Address []:demoserver@invalid

# Dockered proof of concept

## Nginx

    export base_dir=<git clone root of this repo>

    docker pull nginx:1.13.8

    docker run --name mutual-tls-nginx -p 9443:443 -v $base_dir/nginx/nginx.conf:/etc/nginx/nginx.conf:ro -v $base_dir/keys/valid:/certs:ro -d nginx:1.13.8

## Python downstream server

    cd docker/python-dumper && docker build . -t python-dumper

    docker run -p 8000:8000 --name python-dumper -d python-dumper

## Testing the setup

Run in directory keys/valid:

    curl --key client.key --cert client.pem --insecure https://localhost:9443

OR if you prefer httpie

    http --verify=no --ssl=tls1.2 -v --cert-key=client.key --cert=client.pem https://localhost:9443

TODO add instructions how to import client cert to browser and how to demo that.

See response.

Check logs:

    docker logs -f mutual-tls-nginx

And here is an example:

    172.17.0.1 - - [18/Feb/2018:19:01:32 +0000] "GET / HTTP/1.1" 200 768 "Client fingerprint" 26e11c0d2aa55f6e068afa64084a9a39eef5a81a "Client DN" emailAddress=democlient@invalid,CN=democlient.invalid,OU=client poc,O=DemoClient corp,L=Helsinki,C=FI

Downstream server

    docker logs -f python-dumper

And example:

    ERROR:root:X-client-cert-fingerprint: 26e11c0d2aa55f6e068afa64084a9a39eef5a81a
    X-client-cert-dn: emailAddress=democlient@invalid,CN=democlient.invalid,OU=client poc,O=DemoClient corp,L=Helsinki,C=FI
    Host: python-dumper:8000
    Connection: close
    User-Agent: HTTPie/0.9.9
    Accept-Encoding: gzip, deflate
    Accept: */*