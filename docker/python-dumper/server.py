#! /usr/bin/env python3

import http.server
import socketserver
import logging

PORT = 8000

class Server(http.server.SimpleHTTPRequestHandler):

    def do_GET(self):
        logging.error(self.headers)
        fingerprint = self.headers.get('X-client-cert-fingerprint')
        if fingerprint == None:
            self.protocol_version='HTTP/1.1'
            self.send_response(403, 'FORBIDDEN')
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(bytes("<html> <head><title> Nopes </title> </head> <body>No cert fingerprint found!</body></html>", 'UTF-8'))
        else:
            self.protocol_version='HTTP/1.1'
            self.send_response(200, 'OK')
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(bytes("<html> <head><title> Yes </title> </head> <body>yeah</body></html>", 'UTF-8'))

    def serve_forever(port):
        socketserver.TCPServer(('', port), Server).serve_forever()

if __name__ == "__main__":
    Server.serve_forever(PORT)
